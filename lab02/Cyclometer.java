/* Rebecca Davis
Date: 9-7-18
Course:CSE02 f18
Purpose: To study thr storage of variables and numerical values along with basic computations*/
//print the number of minutes for each trip
//print the number of counts for each trip
//print the distance of each trip in miles
//print the distance for the two trips combined
public class Cyclometer{//establishes a class
  
  public static void main(String args[]){
    //establishes a main method
    int secTrip1=320;
    //sets the variable secTrip1 to the value 320
    int secTrip2=474; 
    //sets the variable secTrip2 to the value 474
    int countsTrip1=2389; 
    //sets the variable countsTrip1 to the value 2389
    int countsTrip2=8635; 
    //sets the variable countsTrip2 to the value 8635
    double wheelDiameter= 23.0; 
    //sets the variable wheelDiameter to the value 23.0
    float PI= (float) 3.14159;
    //sets the variable PI to the value 3.14159
    int feetPerMile=4568; 
    //sets the variable feetPerMile to the value 4568
    int inchesPerFoot= 12; 
    //sets the variable inchesPerFoot to the value 12
    int secondsPerMinute= 60;
    //  sets the variable secondsPerMinute to the value 60
    System.out.println("Trip 1 took "+ (secTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    // prints out the info for trip 1
    System.out.println("Trip 2 took " + (secTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); 
    //prints out the info for trip 2
    double distanceTrip1= countsTrip1*wheelDiameter*PI;
    //distance 1 in inches
    double distanceTrip2= countsTrip2*wheelDiameter*PI;
    // distance 2 in inches
    double distanceTrip1Mile= (double) inchesPerFoot*feetPerMile;
    // Gives distance in miles
  	double distanceTrip2Mile= countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    //distance of trip 2 in miles
	  double totalDistance= distanceTrip1+distanceTrip2;
    //gives total distanceTrip1
    System.out.println("Trip 1 was " + distanceTrip1Mile + " miles.");
    //prints milage of trip 1
    System.out.println("Trip 2 was " + distanceTrip2Mile + " miles."); 
    //prints milage of trip 2
    System.out.println("The total distance was " + totalDistance + " miles.");
    // prints total milage
    
    
    
    
  } //closes main method
  
} //closes class
 