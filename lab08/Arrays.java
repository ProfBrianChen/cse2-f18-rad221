/* Rebecca Davis
Date: 11-18-18
Course:CSE02 f18
Purpose: This lab session will get you familiar with one-dimensional arrays.
1)  Create 2 array of integers with each having a size of 100.    
2)    Fill in one of the arrays with randomized integers in the range of 0 to 99, using math.random().
3)  Use the second array to hold the number of occurrences of each number in the first array. 
4)  See sample run and output below. Note for the sample run I am using less than 100 numbers */
import java.util.Random;
import java.util.Scanner;
//imports the random tool
public class Arrays{
  //establishes the class Arrays and makes it public
  public static void main(String[] args){
    //creates the main method and makes it public while accepting any variable type
   random();
    //calls the random() method
    randomFill();
    //calls the randomFill() method
    
    }
  public static int[] random(){
    //creates the random method
    int[] firstArray= new int[100];
    //creates the firstArray array and sets it to the int variable with 100 elements
    for(int i=0;i<firstArray.length;i++)
      //for loop to go iterate through the array
    {
        firstArray[i] = randomFill();
      //as the loop iterates, this fills each place with a random number by calling the randomFill method
     // System.out.print(Arrays.toString(firstArray));
      
    }
    return firstArray;
    //returns the firstArray array
  }
  public static int randomFill(){
    //creates the randomFill method
    Random rand = new Random();
    //creates a random variable and makes room for it
    int randomNum = rand.nextInt();
    //sets the variable randomNum and sets it to a next random int
    return randomNum;
    //returns the variable randomNum to be used in the random method to fill the array
    }//closed the main method
  public static int[] occurances(){
   int[] secondArray= new int[7];
    int [] count = new int[7];
    int i,temp = 0;
     
    for(i = 0; i < secondArray.length; i++){
                temp = secondArray[i];
                count[temp]++;
            }//end of for looop

        for(i=1; i < count.length; i++){

            if(count[i] > 0 && count[i] == 1){
             System.out.println(i+"occurs "+count[i]+" time." );
             }
            else if(count[i] >=2){
                System.out.println(i+"occurs "+count[i]+" times.");
            }
    
            }
    return count;
  }
}



