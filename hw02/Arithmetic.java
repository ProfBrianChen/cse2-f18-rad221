/*Rebecca Davis
Date: 9-9-18
Course: CSE02 f18
Purpose: To practice manipulating data stored in variables, running simple calculations, and printing numerical outputs
Reminders: 
1. Tax= 6%
Calculate:
1.Total cost of each kind of item (i.e. total cost of pants, etc)
2.Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
3.Total cost of purchases (before tax)
4.Total sales tax
5.Total paid for this transaction, including sales tax. 
*/
public class Arithmetic{
  // establishes class called "Arithmetic"
  
  public static void main(String args[]){
    // establishes a main method
    
    int numPants = 3;
    //number of pairs of pants
    double pantsPrice = 34.98;
    //cost per pair of pants
    int numShirts = 2;
    //number of sweatshirts
    double shirtPrice = 24.99; 
    //cost per shirt
    int numBelts = 1;
    //number of belts
    double beltCost = 33.99; 
    //cost per belt
    double paSaleTax = 0.06;
    //the tax rate 
    double totalPants = numPants*pantsPrice;
    // total cost of pants
    double totalShirts = numShirts*shirtPrice;
    // total cost of shirts
    double totalBelts = numBelts*beltCost;
    // total cost of belts
    System.out.println("The total price of pants is " +totalPants + ".");
    // prints the cost of all of the pants w/o tax
    System.out.println("The total price of shirts is " +totalShirts + ".");
    // prints the cost of all of the shirts w/o tax
    System.out.println("The total price of belts is " +totalBelts + ".");
    // prints the cost of all of the belts w/o tax
    double salesPants = paSaleTax*totalPants;
    // sales tax of all pants
    double salesShirts = paSaleTax*totalShirts;
    // sales tax of all shirts
    double salesBelts = paSaleTax*totalBelts;
    // sales tax of all belts
    System.out.println("The tax for all pants is " +salesPants + ".");
    // prints tax for all pants
    System.out.println("The tax for all shirts is " +salesShirts + ".");
    // prints tax for all shirts
    System.out.println("The tax for all belts is " +salesBelts +".");
    // prints tax for all belts
    double totalnotax = totalBelts + totalShirts + totalPants;
    // total cost of purchases before tax
    System.out.println("The total cost of purchases before tax is " +totalnotax +".");
    // prints total cost of purchases before tax
    double totalTax = salesBelts + salesShirts + salesPants;
    // total tax of all items
    System.out.println("The total sales tax is " +totalTax +".");
    // prints total tax of all items
    double totaltra = totalnotax + totalTax;// total cost of whole transaction
    System.out.println("The total paid for this transaction, including tax, is " +totaltra +".");
    // prints total cost of whole transaction
  }// closes class
  
}// closes main method