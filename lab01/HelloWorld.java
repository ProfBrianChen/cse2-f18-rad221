/*Rebecca Davis
Date: 9-1-18
Course:CSE02 f18
Purpose: To practice printing and connecting codeanywhere and bitbucket*/
public class HelloWorld{
  // establishes a class
  
  public static void main(String args[]){
    //establishes a main method
    
    System.out.println("Hello, World");
    //prints "Hello, World" in the terminal
    
  }//closes main method
    
}//closes class
    
    
    