/* Rebecca Davis
Date: 9-21-18
Course:CSE02 f18
Purpose: To practice using if statements, switch statements and the random math generator Math.random*/
//This program generates random card descriptions
/*Remember: numbers 1-52 (inclusive)
-if the card number corresponds to numbers 1-13, they are diamonds
-cards 14-26 are clubs
-27- are hearts
-spades*/
//
import java.lang.Math;
//imports a math class so that the programmer can use the random math generator Math.random
public class CardGenerator{
  //establishes the class CardGenerator
  public static void main(String[] args) {
    //establishes the main method
       int rndm= (int) Math.ceil(Math.random() * 52);
    //sets the varibale rndm
    //math.ceil:this starts the number pool starting at 1 instead of 0
    //math.random*52: this uses the random math generator and makes 52 the max number
       switch(rndm){
         case <14:
           System.out.println("You picked the "+rndm+" of diamonds");
           break;
    
     }
  }
    //if (rndm <= 26)
    //closes the for loop
    
  }
}

