/* Rebecca Davis
Date: 9-14-18
Course:CSE02 f18
The code below takes a bill inputted by the user and splits it evenly by
determining how much each person needs to pay in order to fully take care of the check*/
//
//
 import java.util.Scanner;
// imports the scanner class needed for collecting the input from the user
 public class Check{ 
   // establishes main method needed for a java program to work
   	    public static void main(String[] args) { 
          //establishes the use of strings
        Scanner myScanner = new Scanner( System.in );
          // allows the computer to collect input from the user
        System.out.print("Enter the original cost of the check in the form xx.xx: ");
          // this prints a sentence that the user can input a response
        double checkCost = myScanner.nextDouble();
          //establishes the double checkCost and matches it to the first input of the user
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
          // prints the percentage of the tip that each person has to pay
        double tipPercent = myScanner.nextDouble();
          // establishes the variable of the tip percent and matches it to the second input of the user
        tipPercent /=100; //this is to convert the percentage into a decimal value
        System.out.print("Enter the number of people who went out to dinner: ");
          // prints the command at which the user can enter the number of people present at dinner
        int numPeople=  myScanner.nextInt();
          // matches the double numPeople to the inout of the user according to how many people were at dinner
        double totalCost;
          //establishes the double totalCost
        double costPerPerson; 
          // establishes the double costPerPerson
        totalCost= checkCost*(1 + tipPercent);
          //takes the checkCost and the tip to create the total cost of dinner
        costPerPerson= totalCost / (double) numPeople;
          // takes the cost and divides it by the number of people to calculate how much each person pays for the night
        int dimes= (int) (costPerPerson*10) % 10;
          //sets the number of dimes and the amount 
        int  pennies= (int) (costPerPerson*100) %10;
          //sets the number of pennies and the amount
        int dollars = (int) costPerPerson;
          //sets the amount of dollars and the amount
        System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);// prints the price of each persons individual bill
          
          
}  //end of main method   
  	} //end of class

