/*Rebecca Davis
Date: 9-4-18
Course:CSE02 f18
Purpose: To practice formatting code so that it is easy to read and documenting details.*/

public class WelcomeClass{
  //establishes a class
  
  public static void main(String args[]){
    //establishes a main method
    
    System.out.println("-----------");
    // prints the dashes I need for the shape
    System.out.println("| WELCOME |");
    // prints the | WELCOME | of the shape
    System.out.println("-----------");
    // prints the dashes for the shape
    System.out.println("  ^  ^  ^  ^  ^  ^");
    // prints ^ for the shape
    System.out.println("// \\/ \\/ \\/ \\/ \\/ //");
    // prints the slashes for the shape
    System.out.println("<-R--A--D--2--2--1->");// prints my Lehigh Network ID
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    // prints the slashes for the shape
    System.out.println("  v  v  v  v  v  v");
    // prints the v for the shape
    System. out.println(" My name is Rebecca Davis and I am from Newark, New Jersey. I am a physics major.");
    //prints my bio
  }//closes class
  
}//closes main method