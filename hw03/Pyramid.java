/*Rebecca Davis
Date: 9-14-18
Course: CSE02 f18
Purpose: To practice writing code that lets the user input data and allows arithmetic operations
1.ask for dimensions
2.calculate the volume */
import java.util.Scanner;
// imports the scanner class insiede java.util
public class Pyramid{
  // establishes the class 'Pyramid' and makes it public
  public static void main(String args[]) {
    // establishes strings
    Scanner myScanner = new Scanner(System.in);
    // allows the computer to get inputs using the scanner class
    System.out.print("Enter the length of the square side of the pyramid: ");
    // prints the command to allow the user to input the length of the pyramid's side
    double pyraSide= myScanner.nextDouble();
    // establishes the double pyraSide and matches it to the user's first input
    System.out.print("Enter the the height of the pyramid: ");
    // prints the command to allow the user to input the height of the pyramid
    double pyraHeight= myScanner.nextDouble();
    // establishes the double pyraHeight and matches the user's second input to it
    double pyraWidth= pyraSide*pyraSide;
    // establishes the double pyraWidth and sets it as the value of  pyraSideSide^2 because the base of the pyramid is a square
    double pyraVolume= (pyraWidth*pyraHeight)/(3);
    // the equation for the volume of a pyramid. this line establishes it as a double
    System.out.println("Volume is: " +pyraVolume);
    // this prints the volume of the pyramid depending on the user's inputs
    
    
    
    
  }//closes the main method
  
}// closes the class
    