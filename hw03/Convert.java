/*Rebecca Davis
Date: 9-14-18
Course: CSE02 f18
Purpose: To practice writing code that lets the user input data and allows arithmetic operations
Assingment: Hurricanes drop a tremendous amount of water on areas of land. 
Write a program that asks the user
for doubles that represent the number of acres of land affected by 
hurricane precipitation and how many inches of rain were dropped on average. 
Convert the quantity of rain into cubic miles.*/

import java.util.Scanner;
// ^^imports a scanner class
public class Convert{
  //^^ establishes the 'Convert' and makes it public
  public static void main(String[] args) {
    //^^establishes the use of strings
    Scanner myScanner = new Scanner( System.in );
    //^^allows the computer to collect input from the user
    System.out.print("Enter the affected area in acres: ");
    //^^prints the line that allows the user to enter the number of acres
    double numAcres = myScanner.nextDouble();
    //^^establishes the double numAcres and matches it to the input of acres from the user
    System.out.print("Enter the rainfall in the affected area in inches: ");
    //^^prints the line that allows the user to enter the number of inches
    double numInches = myScanner.nextDouble();
    //^^establishes the double numInches and matches it to the input of inches from the user
    double acreInch= numInches*numAcres;
    //^^calculates the amount of acre inches by multiplying the two inputs from the user, the number of acres and the number of inches
   // System.out.println("Acre inches are " + acreInch);
    //^^prints out the amount of acre inches
    double acretoGallons = 27154.285990761;
    //^^ establishes the double acretoGallons, the constant that is the conversion factor to turn acre inches to gallons
    double gallons =acreInch*27154.285990761;
    //^^ establishes the number of gallons by multiplying the number of acres by the conversion factor
    //System.out.println(gallons);
    //^^prints the number of gallons that were calculated above
    double gallonstoMiles=1101117147428.57; 
    //^^establishes the constant that is the conversion factor of gallons to miles
    double miles= gallons/gallonstoMiles;
    //^^ establishes the double of the cubic miles, which is found by dividing the number of gallons by the conversion factor
    System.out.println("Cubic Miles: " + miles);
    //^^prints the number of cubic miles affected by the rain
    
    
    
    
    
    
    
    
    
  }//closes the main method
  
}// closes the class
    
  
