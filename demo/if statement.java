import java.util.Scanner;

public class verizonNotifications{
	public static void main(String[] args){
		//boolean notifyMe;
		int notifyMe;
		//int notifyMe, notifyMe95, notifyMeLater;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Please indicate 'true' to opt in to data warnings. To opt out, indicate 'false'.");
		notifyMe = myScanner.nextBoolean();
		//System.out.println("Please indicate '1' to opt in to data warnings. To opt out, indicate '2'.");
		//notifyMe = myScanner.nextInt();
		//System.out.println("If you opted in, please indicate '3' to receive data warnings at 95% rather than the default 75%.");
		notifyMe95 = myScanner.nextInt();
		
		// Uncomment to show how scope will be affected
		//if (notifyMe == 2){
		//System.out.println("If you opted out, do you want to be reminded of these notification at a later time? If so, please indicate '4'.");
		//notifyMeLater = myScanner.nextInt();
		
		
		// If the customer sets the notifcations on, output "You've signed up!"
		// Otherwise, output "You may always opt in at a later time.
		if (notifyMe){
			System.out.println("You've signed up!");
		}
		else {
			System.out.println("You may always opt in at a later time.");
		}
		/*if (notifyMe == 1){
			System.out.println("You've signed up!");
		}
		else if (notifyMe == 2){
			System.out.println("You may always opt in at a later time.");
		}*/
		
		// Ternary Operator
		//String stringOut = (notifyMe) ? "You've signed up!" : "You may always opt in at a later time";
		//System.out.println(stringOut);
		
		// Example of nested if-else
		/*if (notifyMe == 1){
			System.out.println("You've signed up!");
			if (notifyMe95 == 3){
				System.out.println("Verizon will notify you when you've reached 95% of your monthly data rather than the defaulct 75%.");
			}
		}
		else if (notifyMe == 2) {
			System.out.println("You may always opt in at a later time.");
			if (notifyMeLater == 4){
				System.out.println("Verizon will remind you of these notifications in one month.");
			}
			else{
				System.out.println("Verizon will not send you notifications and will no longer remind you of them.");
			}
		}*/
		
		//myScanner.nextLine();
		//myScanner.nextLine();
		// How does the following evaluate?
		//if (notifyMe == 1 && notifyMe95 == 3){ //((notifyMe == 1) && (notifyMe95 == 3))
			//System.out.println("Since you are receiving messages at a high percentage, we will make an internal note on how many times this occurs.  If it occurs often, we may send you information on upping your data allowance.");
			
		}
	}
	
}
