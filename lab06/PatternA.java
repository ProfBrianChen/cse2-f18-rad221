/*Rebecca Davis
Date: 10-12-18
Course: CSE02 f18
Purpose: To practice writing code that uses nested if statements.
The objective is to use nested if statements to print specific patterns of integers.
*/
import java.util.Scanner;
//imports the scanner class
public class PatternA{
  //establishes the class "PatternA" and makes it public, meaning that it can be accessed anywhere in the code.
  public static void main(String[] args) {
    Scanner myScanner= new Scanner (System.in);
    System.out.print("Enter an integer from 1-10: ");
    //requests that the user inputs an integer from 1-10
    int pyraLength= myScanner.nextInt();
    //sets the variable pyraLength to the first int that the user inputs and casts it as an int
    //System.out.println(pyraLength);to check if pyraLength prints as what I want it to be 
    if (pyraLength>10){
      System.out.println("Error. Please enter an integer from 1-10");
    }
    //checks to see if the input is between 1 and 10. If it isnt then the request prints again.
    if (pyraLength<1){
      System.out.println("Error. Please enter an integer from 1-10");
    }
    //checks to see if the input is between 1 and 10. If it isnt then the request is printed again.
    int starInt=1;
    //establishes the varible starInt and casts it as an int
    //System.out.print(starInt);to check if starInt prints as what I want it to be 
    ///here needs to go the loop that takes starInt and increases it AND the number of rows/pyraLength until the requirement is met.
    
    
   
    
    
  }

}





